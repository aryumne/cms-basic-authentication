<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('general_settings', function (Blueprint $table) {
            $table->uuid('id');
            $table->text('brand_img')->nullable();
            $table->text('address')->nullable();
            $table->string('sosial_media_title')->nullable();
            $table->json('instagram_media')->nullable();
            $table->json('facebook_media')->nullable();
            $table->json('twitter_media')->nullable();
            $table->json('youtube_media')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('footer_text')->nullable();
            $table->string('galery_title')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('general_settings');
    }
};
