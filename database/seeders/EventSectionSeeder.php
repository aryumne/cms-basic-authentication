<?php

namespace Database\Seeders;

use App\Models\EventSection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('event_sections')->truncate();
        EventSection::create([
            'event_title'   => 'JOIN PHOTO COMPETITION!',
            'event_desc'    => '<p>Untuk Ikut Photo Competition ini, caranya sebagai berikut :</p><ol><li>Posting Foto Menarik kamu di social media Instagram di feed kamu dengan foto bersama OLI Yamalube Power Matic dan Super Matic<br>jangan lupa untuk Sobek Label</li><li>&nbsp;Follow @aslilebihbaik dan like posting berikut klik disini</li><li>&nbsp;Ceritakan di caption pengalaman kamu saat cek keaslian produk<br>yamalube melalui website yamalubepromo.com</li><li>Mention dan Tag @aslilebihbaik sertakan hashtag #Yamalube<br>#OliAslidariYamaha #YamalubeGoesToMotogp2023 da hashtag<br>#YGTMMekanik khusus mekanik serta hashtag #YGTMCustomer<br>khusus Konsumen.</li><li>&nbsp;Mention 3 orang temanmu dan ajak mereka ikutan</li><li>Pastikan akun instagram kamu tidak di private</li><li>Setiap Bulannya tim Yamalube akan memilih 1 orang beruntung<br>untuk mendapatkan hadiah bulan dan di akhir periode akan di undi<br>untuk mendapatkan hadiah tiket nonton langsung Moto GP di<br>Mandalika</li></ol>',
            'event_desc2'   => 'Keputusan Juri bersifat mutlak dan tidak dapat diganggu gugat.
Periode berlangsung hingga 31 Agustus 2023.',
            'event_src_img'     =>'images/event-img.png',
            'event_alt_img' => 'event-img.png'
        ]);
    }
}
