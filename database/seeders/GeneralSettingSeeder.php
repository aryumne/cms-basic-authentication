<?php

namespace Database\Seeders;

use App\Models\GeneralSetting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneralSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('general_settings')->truncate();
        GeneralSetting::create([
            'brand_img' => 'icons/brand-logo.png',
            'address'   => 'YAMAHA INDONESIA MOTOR MANUFACTURING
JL. DR KRT RADJIMAN WIDYODININGRATCAKUNG JAKARTA TIMUR 13920 INDONESIA',
            'sosial_media_title'=> 'Follow dan Ikuti Social Media Yamalube',
            'instagram_media'   => [
                'icon'  => 'icons/ic-instagram.png',
                'url'   => 'http://www.instagram.com'
            ],
            'facebook_media'   => [
                'icon'  => 'icons/ic-facebook.png',
                'url'   => 'http://www.facebook.com'
            ],
            'twitter_media'   => [
                'icon'  => 'icons/ic-twitter.png',
                'url'   => 'http://www.twitter.com'
            ],
            'youtube_media'   => [
                'icon'  => 'icons/ic-youtube.png',
                'url'   => 'http://www.youtube.com'
            ],
            'phone' => '08111080090',
            'email' => 'contact_center@yamaha-motor.co.id',
            'footer_text'   => 'Copyrights © 2023 All Rights Reserved by Yamaha Motor Indonesia',
            'galery_title'  => 'Gallery Participant'
        ]);
    }
}
