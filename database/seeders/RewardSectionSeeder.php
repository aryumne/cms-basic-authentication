<?php

namespace Database\Seeders;

use App\Models\RewardSection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RewardSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('reward_sections')->truncate();
        RewardSection::create([
            'reward_title' => 'Menangkan Hadiah Menarik',
            'card_list' => [
                [
                    'reward_src_img' => 'images/ticket-reward.png',
                    'reward_alt_img' => 'ticket_reward.png',
                    'reward_card_desc' => 'Ticket Moto GP 2023 untuk 15 Mekanik & 5 Konsumen'
                ],
                [
                    'reward_src_img' => 'images/kaos-reward.png',
                    'reward_alt_img' => 'kaos_reward.png',
                    'reward_card_desc' => '2 POLO Shirt Bertanda tangan Fabio Quartararo dan Franco Morbidelli'
                ]
            ]
        ]);
    }
}
