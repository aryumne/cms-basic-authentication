<?php

namespace Database\Seeders;

use App\Models\Section;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('sections')->truncate();
        $sections = [
            [
                'display_name' => 'Header Section',
                'path_name'    => 'header-section'
            ],
            [
                'display_name' => 'Event Section',
                'path_name'    => 'event-section'
            ],
            [
                'display_name' => 'Reward Section',
                'path_name'    => 'reward-section'
            ],
            [
                'display_name' => 'FAQ Section',
                'path_name'    => 'faq-section'
            ],
            [
                'display_name' => 'General Settings',
                'path_name'    => 'general-setting'
            ],
        ];

        foreach ($sections as $sec)
        {
            Section::create($sec);
        }
    }
}
