<?php

namespace Database\Seeders;

use App\Models\FaqSection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FaqSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('faq_sections')->truncate();
        $dummies = [
            [
                'faq_title' => 'Frequently Asked Questions'
            ],
            [
                'faq_title' => 'Frequently Asked Questions',
                'faq_question' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus, autem?',
                'faq_answer' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis molestias quam, exercitationem aut veniam tempore tenetur aliquid ea placeat inventore!'
            ],
            [
                'faq_title' => 'Frequently Asked Questions',
                'faq_question' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus, autem?',
                'faq_answer' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis molestias quam, exercitationem aut veniam tempore tenetur aliquid ea placeat inventore!'
            ],
            [
                'faq_title' => 'Frequently Asked Questions',
                'faq_question' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus, autem?',
                'faq_answer' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis molestias quam, exercitationem aut veniam tempore tenetur aliquid ea placeat inventore!'
            ],
            [
                'faq_title' => 'Frequently Asked Questions',
                'faq_question' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus, autem?',
                'faq_answer' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis molestias quam, exercitationem aut veniam tempore tenetur aliquid ea placeat inventore!'
            ],
            [
                'faq_title' => 'Frequently Asked Questions',
                'faq_question' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ducimus, autem?',
                'faq_answer' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis molestias quam, exercitationem aut veniam tempore tenetur aliquid ea placeat inventore!'
            ],
        ];

        foreach ($dummies as $dummy)
        {
            FaqSection::create($dummy);
        }
    }
}
