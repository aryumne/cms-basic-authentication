<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GaleryController;
use App\Http\Controllers\GeneralSettingController;
use App\Http\Controllers\Section\FaqSectionController;
use App\Http\Controllers\Auth\AuthenticationController;
use App\Http\Controllers\Section\EventSectionController;
use App\Http\Controllers\Section\HeaderSectionController;
use App\Http\Controllers\Section\RewardSectionController;


Route::middleware(['redirectIfAuthenticated'])->group(function () {
    Route::get('/login', [AuthenticationController::class, 'login'])->name('login');
    Route::post('/login', [AuthenticationController::class, 'authenticate'])->name('authenticate');
});
Route::middleware(['auth'])->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('dashboard');
    Route::prefix('section')->group(function () {
        Route::get('/header-section', [HeaderSectionController::class, 'index'])->name('header-section');
        Route::post('/header-section', [HeaderSectionController::class, 'store'])->name('header-section.store');
        Route::patch('/header-section/{uuid}', [HeaderSectionController::class, 'update'])->name('header-section.update');
        Route::delete('/header-section/{uuid}', [HeaderSectionController::class, 'destroy'])->name('header-section.destroy');
        Route::get('/event-section', [EventSectionController::class, 'index'])->name('event-section');
        Route::patch('/event-section/{uuid}', [EventSectionController::class, 'update'])->name('event-section.update');
        Route::get('/reward-section', [RewardSectionController::class, 'index'])->name('reward-section');
        Route::patch('/reward-section/{uuid}', [RewardSectionController::class, 'update'])->name('reward-section.update');
        Route::get('/faq-section', [FaqSectionController::class, 'index'])->name('faq-section');
        Route::post('/faq-section', [FaqSectionController::class, 'store'])->name('faq-section.store');
        Route::patch('/faq-section/{uuid}', [FaqSectionController::class, 'update'])->name('faq-section.update');
        Route::delete('/faq-section/{uuid}', [FaqSectionController::class, 'destroy'])->name('faq-section.destroy');
        Route::get('/general-setting', [GeneralSettingController::class, 'index'])->name('general-setting');
        Route::patch('/general-setting/{uuid}', [GeneralSettingController::class, 'update'])->name('general-setting.update');
    });
    Route::resource('galery', GaleryController::class);
    Route::post('/logout', [AuthenticationController::class, 'logout'])->name('logout');
});

