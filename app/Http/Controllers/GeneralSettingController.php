<?php

namespace App\Http\Controllers;

use App\Models\GeneralSetting;
use Illuminate\Http\Request;

class GeneralSettingController extends Controller
{
    public function index()
    {
        $data = GeneralSetting::first();
        return view('auth-pages.sections.general-setting', [
            'data' => $data
        ]);
    }

    public function update(Request $request, $uuid)
    {
        $data = GeneralSetting::find($uuid);
        if (!$data) return back('warning', 'Data not found!');
        $data->address  = $request->address;
        $data->phone    = $request->phone;
        $data->email    = $request->email;
        $data->footer_text  = $request->footer_text;
        $data->galery_title = $request->galery_title;
        $instagram_icon = $data->instagram_media['icon'];
        $facebook_icon  = $data->facebook_media['icon'];
        $twitter_icon   = $data->twitter_media['icon'];
        $youtube_icon   = $data->youtube_media['icon'];

        if ($request->hasFile('instagram_icon')) {
            $fileinsta  = $request->file('instagram_icon');
            $fninsta    = $fileinsta->getClientOriginalName();
            $fileinsta->storeAs('icons/', str_replace(" ", "-", $fninsta));
            $instagram_icon  = "icons/" . str_replace(" ", "-", $fninsta);
        }
        $data->instagram_media = [
            'icon'  => $instagram_icon,
            'url'   => $request->instagram_url ?? $data->instagram_url
        ];
        if ($request->hasFile('facebook_icon')) {
            $filefb     = $request->file('facebook_icon');
            $fnfb       = $filefb->getClientOriginalName();
            $filefb->storeAs('icons/', str_replace(" ", "-", $fnfb));
            $facebook_icon  = "icons/" . str_replace(" ", "-", $fnfb);
        }
        $data->facebook_media = [
            'icon'  => $facebook_icon,
            'url'   => $request->facebook_url ?? $data->facebook_url
        ];
        if ($request->hasFile('twitter_icon')) {
            $filetwit   = $request->file('twitter_icon');
            $fntwit     = $filetwit->getClientOriginalName();
            $filetwit->storeAs('icons/', str_replace(" ", "-", $fntwit));
            $twitter_icon  = "icons/" . str_replace(" ", "-", $fntwit);
        }
        $data->twitter_media = [
            'icon'  => $twitter_icon,
            'url'   => $request->twitter_url ?? $data->twitter_url
        ];
        if ($request->hasFile('youtube_icon')) {
            $fileyt = $request->file('youtube_icon');
            $fnyt   = $fileyt->getClientOriginalName();
            $fileyt->storeAs('icons/', str_replace(" ", "-", $fnyt));
            $youtube_icon  = "icons/" . str_replace(" ", "-", $fnyt);
        }
        $data->youtube_media = [
            'icon'  => $youtube_icon,
            'url'   => $request->youtube_url ?? $data->youtube_url
        ];

        $data->save();
        return back()->with('primary', 'Data updated successfully');

    }
}
