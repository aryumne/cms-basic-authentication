<?php

namespace App\Http\Controllers;

use App\Models\Section;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index($path = null) 
    {
        return redirect()->route('header-section');
    }
}
