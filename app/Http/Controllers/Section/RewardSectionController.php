<?php

namespace App\Http\Controllers\Section;

use Illuminate\Http\Request;
use App\Models\RewardSection;
use App\Http\Controllers\Controller;

class RewardSectionController extends Controller
{
    public function index()
    {
        $data = RewardSection::first();
        return view('auth-pages.sections.reward-section', [
            'data' => $data
        ]);
    }

    public function update(Request $request, $uuid)
    {
        $data = RewardSection::find($uuid);
        if (!$data) return back('warning', 'Data not found!');
        $data->reward_title = $request->reward_title;
        $src1 = $data->card_list[0]['reward_src_img'];
        $alt1 = $data->card_list[0]['reward_alt_img'];
        $desc1 = $request->reward_card_desc1;
        $src2 = $data->card_list[1]['reward_src_img'];
        $alt2 = $data->card_list[1]['reward_alt_img'];
        $desc2 = $request->reward_card_desc2;
        if ($request->hasFile('reward_card_img1')) {
            $cardimg1 = $request->file('reward_card_img1');
            $filename1 = $cardimg1->getClientOriginalName();
            $cardimg1->storeAs('images/', str_replace(" ", "-", $filename1));
            $src1  = "images/" . str_replace(" ", "-", $filename1);
            $alt1  = $filename1;
        }
        if ($request->hasFile('reward_card_img2')) {
            $cardimg2 = $request->file('reward_card_img2');
            $filename2 = $cardimg2->getClientOriginalName();
            $cardimg2->storeAs('images/', str_replace(" ", "-", $filename2));
            $src2  = "images/" . str_replace(" ", "-", $filename2);
            $alt2  = $filename2;
        }
        $data->card_list = [
            [
                'reward_src_img' => $src1,
                'reward_alt_img' => $alt1,
                'reward_card_desc' => $desc1
            ],
            [
                'reward_src_img' => $src2,
                'reward_alt_img' => $alt2,
                'reward_card_desc' => $desc2
            ]
        ];
        $data->save();
        return back()->with('primary', 'Data updated successfully');
    }
}
