<?php

namespace App\Http\Controllers\Section;

use App\Models\Section;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EventSection;

class EventSectionController extends Controller
{
    public function index()
    {
        $data = EventSection::first();
        return view('auth-pages.sections.event-section', [
            'data' => $data
        ]);
    }

    public function update(Request $request, $uuid) {
        $data = EventSection::findOrFail($uuid);
        if (!$data) return back()->with('warning', 'Data not found!');
        $data->event_title = $request->event_title ?? $data->event_title;
        $data->event_desc = $request->event_desc ?? $data->event_desc;
        $data->event_desc2 = $request->event_desc2 ?? $data->event_desc2;
        if($request->hasFile('event_src_img')) {
            $file = $request->file('event_src_img');
            $filename = $file->getClientOriginalName();
            $src  = "images/".str_replace(" ", "-", $filename);
            $file->storeAs('images/', str_replace(" ", "-", $filename));
            $data->event_src_img = $src;
            $data->event_alt_img = str_replace(" ", "-", $filename);
        }
        $data->save();
        return back()->with('primary', 'Data updated successfully');
    }
}
