<?php

namespace App\Http\Controllers\Section;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FaqSection;

class FaqSectionController extends Controller
{
    public function index()
    {
        $data = FaqSection::first();
        $faqs = FaqSection::all();
        return view('auth-pages.sections.faq-section', [
            'data' => $data,
            'faqs' => $faqs
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'faq_question' => 'required',
            'faq_answer' => 'required'
        ]);

        $firstfaq = FaqSection::first();
        if(!$firstfaq) return back()->with('warning', 'Data not found!');
        FaqSection::create([
            'faq_title' => $firstfaq->faq_title,
            'faq_question' => $request->faq_question,
            'faq_answer' => $request->faq_answer
        ]);

        return back()->with('primary', 'Data has been successfully created!');
    }

    public function update(Request $request, $uuid)
    {
        $data = FaqSection::first();

        if($data->id != $uuid) {
            $request->validate([
                'faq_question' => 'required',
                'faq_answer' => 'required'
            ]);
        }
        $data = FaqSection::find($uuid);
        if (!$data) return back()->with('warning', 'Data not found!');
        $title = $request->has('faq_title') ? $request->faq_title : $data->faq_title;
        $data->faq_title = $title;
        $data->faq_question = $request->faq_question;
        $data->faq_answer = $request->faq_answer;
        $data->save();

        return back()->with('primary', 'Data successfully updated!');
    }

    public function destroy($uuid)
    {
        $data = FaqSection::find($uuid);
        if (!$data) return back()->with('warning', 'Data not found!');
        $data->delete();
        return back()->with('primary', 'Data successfully deleted!');
    }
}
