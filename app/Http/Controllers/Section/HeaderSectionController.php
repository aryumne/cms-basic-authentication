<?php

namespace App\Http\Controllers\Section;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\HeaderSection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class HeaderSectionController extends Controller
{
    public function index()
    {
        $data = HeaderSection::all();
        return view('auth-pages.sections.header-section', [
            'data' => $data
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|file|mimes:jpg,png|max:2048',
        ]);
        $file = $request->file('file');
        $filename = $file->getClientOriginalName();
        $path = $file->storeAs('carousel/',str_replace(" ","-",$filename));
        HeaderSection::create([
            'header_src_img' => $path,
            'header_alt_img' => $filename
        ]);
        return back()->with('primary', 'Image has been successfully uploaded');
    }

    public function update(Request $request, $uuid)
    {
        $request->validate([
            'header_src_img' => 'required|file|mimes:jpg,png|max:2048',
        ]);
        $item = HeaderSection::find($uuid);
        if(!$item) return back('warning', 'Data not found!');
        if ($request->hasFile('header_src_img')) {
            $file = $request->file('header_src_img');
            $filename = $file->getClientOriginalName();
            $src = 'carousel/'.str_replace(" ","-",$filename);
            $file->storeAs('carousel/',str_replace(" ","-",$filename));
            $item->header_src_img = $src;
            $item->header_alt_img = $filename;
            $item->save();
            return back()->with('primary', 'Image has been successfully updated');
        }
    }
    
    public function destroy($uuid)
    {
        $banner = HeaderSection::findOrFail($uuid);
        $banner->delete();
        return back()->with('primary', 'Data deleted successfully');
    }
}
