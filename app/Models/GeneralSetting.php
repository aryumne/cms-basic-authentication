<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class GeneralSetting extends Model
{
    use HasUuids, HasFactory;
    protected $guarded = ['id'];
    protected $casts = [
        'instagram_media' => 'array',
        'facebook_media' => 'array',
        'twitter_media' => 'array',
        'youtube_media' => 'array',
    ];
}
