<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RewardSection extends Model
{
    use HasUuids, HasFactory;
    protected $guarded = ['id'];
    protected $casts = ['card_list' => 'array'];
}
