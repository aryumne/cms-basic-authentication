<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FaqSection extends Model
{
    use HasUuids, HasFactory;
    protected $guarded = ['id'];
}
