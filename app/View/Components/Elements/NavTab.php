<?php

namespace App\View\Components\Elements;

use App\Models\Section;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class NavTab extends Component
{

    public $items;
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        $this->items = Section::all();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.elements.nav-tab');
    }
}
