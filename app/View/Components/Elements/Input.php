<?php

namespace App\View\Components\Elements;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Input extends Component
{

    public $type;
    public $nameID;
    public $label;
    public $value;
    public $placeholder;
    public $required;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $nameID,
        $label,
        $value = null,
        $placeholder = '',
        $type = 'text',
        $required = false
    )
    {
        $this->type = $type;
        $this->nameID = $nameID;
        $this->label = $label;
        $this->value = $value;
        $this->placeholder = $placeholder;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.elements.input');
    }
}
