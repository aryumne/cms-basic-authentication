@php
    $currentUrl = url()->current();
    $parsedUrl = parse_url($currentUrl);
    $path = Str::substr($parsedUrl['path'],1);
@endphp
<div class="col-12">
    <ul class="nav nav-pills flex-column flex-md-row mb-3">
        @foreach ($items as $sec)
            <li class="nav-item">
                <a class="nav-link {{ ($sec->path_name == $path ? 'active' : $path == null && $loop->first) ? 'active' : '' }} "
                    href="{{ route($sec->path_name) }}"> {{ $sec->display_name }}</a>
            </li>
        @endforeach
    </ul>
</div>
