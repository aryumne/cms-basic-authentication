
<div {{ $attributes->class(['mb-3 ']) }}>
    <label for="{{ $nameID }}" class="form-label">{{ $label }}</label>
    <input type="{{ $type }}" class="form-control" id="{{ $nameID }}" placeholder="{{ $placeholder }}" name="{{ $nameID }}"
        value="{{ old($nameID, $value) }}" @required($required)/>
</div>
