<nav class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
    id="layout-navbar">
    <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
        <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
            <i class="bx bx-menu bx-sm"></i>
        </a>
    </div>

    <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
        <ul class="navbar-nav flex-row align-items-center ms-auto">
            <li class="nav-item navbar-dropdown dropdown-user dropdown">
                <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                    <div class="avatar avatar-online">
                        <svg height="40px" width="40px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512" xml:space="preserve" fill="#000000"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <path style="fill:#8AD5DD;" d="M256,512c141.384,0,256-114.616,256-256S397.384,0,256,0S0,114.616,0,256S114.616,512,256,512z M256,22.704c128.64,0,233.296,104.656,233.296,233.296c0,128.648-104.656,233.304-233.296,233.304S22.704,384.648,22.704,256 C22.704,127.36,127.36,22.704,256,22.704z"></path> <path style="fill:#8AD5DD;" d="M256,481.304c124.224,0,225.296-101.072,225.296-225.304c0-124.232-101.072-225.296-225.296-225.296 S30.704,131.768,30.704,256S131.776,481.304,256,481.304z M216.2,243.864H256h39.8L256,331.968L216.2,243.864z M371.928,297.96 c0,53.328,0,65.688,0,65.688s-113.608,0-113.224,0l55.256-119.792C313.96,243.864,371.928,244.64,371.928,297.96z M256,116.344 c31.584,0,57.192,25.608,57.192,57.192S287.584,230.728,256,230.728s-57.192-25.608-57.192-57.192S224.416,116.344,256,116.344z M140.072,297.96c0-53.328,57.96-54.096,57.96-54.096l55.256,119.792c0.384,0-113.224,0-113.224,0S140.072,351.288,140.072,297.96z "></path> </g> <g> <path style="fill:#FFFFFF;" d="M256,489.304c128.64,0,233.296-104.656,233.296-233.304C489.296,127.36,384.64,22.704,256,22.704 S22.704,127.36,22.704,256C22.704,384.648,127.36,489.304,256,489.304z M256,30.704c124.224,0,225.296,101.072,225.296,225.296 c0,124.232-101.072,225.304-225.296,225.304S30.704,380.232,30.704,256S131.776,30.704,256,30.704z"></path> <circle style="fill:#FFFFFF;" cx="256" cy="173.536" r="57.192"></circle> </g> <g> <path style="fill:#2D2D2D;" d="M198.04,243.864c0,0-57.96,0.776-57.96,54.096s0,65.688,0,65.688s113.608,0,113.224,0 L198.04,243.864z"></path> <path style="fill:#2D2D2D;" d="M371.928,363.656c0,0,0-12.368,0-65.688s-57.96-54.096-57.96-54.096l-55.256,119.792 C258.32,363.656,371.928,363.656,371.928,363.656z"></path> </g> <polygon style="fill:#DB2B42;" points="256,243.864 216.2,243.864 256,331.968 295.8,243.864 "></polygon> </g></svg>
                    </div>
                </a>
                <ul class="dropdown-menu dropdown-menu-end">
                    <li>
                        <div class="dropdown-divider"></div>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">
                            <i class="bx bx-user me-2"></i>
                            <span class="align-middle">My Profile</span>
                        </a>
                    </li>
                    <li>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button type="submit" class="dropdown-item">
                            <i class="bx bx-power-off me-2"></i>
                            <span class="align-middle">Log Out</span>
                        </button>
                        </form>
                        
                    </li>
                </ul>
            </li>
            <!--/ User -->
        </ul>
    </div>
</nav>
