<x-layouts.auth-layout>
    <div class="row">
        <x-elements.nav-tab />
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>Header Section</h4>
                </div>
                <div class="card-body">
                    <div class="row mb-4">
                        <x-elements.alert />
                        <form class="form" method="POST" action="{{ route('header-section.store') }}"
                            enctype="multipart/form-data">
                            @csrf
                            <label class="form-label">New carousel item</label>
                            <div class="input-group">
                                <input type="file" class="form-control" id="inputGroupFile04"
                                    aria-describedby="inputGroupFileAddon04" name="file" aria-label="Upload"
                                    required>
                                <button class="btn btn-outline-primary" type="submit" id="inputGroupFileAddon04">Upload
                                    new</button>
                            </div>
                        </form>
                    </div>
                    <div class="row row-cols-1 row-cols-md-3 g-4">
                        @foreach ($data as $dt)
                            <div class="col">
                                <div class="card">
                                    <div style="height: 250px; overflow:hidden;"
                                        class="bg-label-secondary d-flex align-items-center justify-content-center">
                                        <img src="{{ asset('storage/'.$dt->header_src_img) }}" class="card-img-top"
                                            alt="{{ $dt->header_alt_img }}">
                                    </div>
                                    <div class="card-body">
                                        <x-elements.button type="button" color="warning" class="mb-2"
                                            data-bs-toggle="modal" data-bs-target="#editform-{{ $dt->id }}">Edit
                                        </x-elements.button>
                                        <form action="{{ route('header-section.destroy', $dt->id) }}" method="POST"
                                            id="delete-form-{{ $dt->id }}" class="d-inline-block">
                                            @csrf
                                            @method('DELETE')
                                            <x-elements.button type="submit" color="danger" class="mb-2">Delete
                                            </x-elements.button>
                                        </form>

                                        {{-- Modal --}}
                                        <div class="modal fade" id="editform-{{ $dt->id }}" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel1">Change Image
                                                        </h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <form class="form"
                                                        action="{{ route('header-section.update', $dt->id) }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PATCH')
                                                        <div class="modal-body">
                                                            <div class="input-group">
                                                                <input type="file" class="form-control"
                                                                    id="header_src_img"
                                                                    aria-describedby="inputGroupFileAddon04"
                                                                    name="header_src_img" aria-label="Upload" required>
                                                                <button class="btn btn-outline-primary" type="submit"
                                                                    id="inputGroupFileAddon04">Upload</button>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <x-elements.button type="submit" color="primary"
                                                                class="mb-2">Save changes
                                                            </x-elements.button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- End Modal --}}
                                    </div>
                                </div>
                            </div>
                            @push('scripts')
                                <script type="text/javascript">
                                    document.addEventListener('DOMContentLoaded', function() {
                                        let form = document.getElementById('delete-form-{{ $dt->id }}');

                                        form.addEventListener('submit', function(event) {
                                            event.preventDefault();
                                            let itemId = form.getAttribute('data-id');

                                            Swal.fire({
                                                title: "Are you sure?",
                                                text: "You won't be able to revert this!",
                                                icon: "warning",
                                                showCancelButton: !0,
                                                confirmButtonText: "Yes, delete it!",
                                                customClass: {
                                                    confirmButton: "btn btn-primary me-3",
                                                    cancelButton: "btn btn-label-secondary",
                                                },
                                                buttonsStyling: !1,
                                            }).then(function(t) {
                                                t.value && form.submit();
                                            });

                                        });
                                    });
                                </script>
                            @endpush
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Preview</h4>
                </div>
                <div class="card-body">
                    <div id="carouselSection" class="carousel slide col-md-8 offset-md-2" data-bs-ride="carousel">
                        <ol class="carousel-indicators">
                             @foreach ($data as $dt)
                                <li data-bs-target="#carouselSection" data-bs-slide-to="{{ $loop->index }}" class="{{ $loop->index == 0 ? 'active' : '' }}"></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @foreach ($data as $dt)
                                <div class="carousel-item {{ $loop->index == 0 ? 'active' : '' }}">
                                    <img class="d-block w-100" src="{{ asset('storage/'.$dt->header_src_img) }}"
                                        alt="{{ $dt->header_alt_img }}" />
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselSection" role="button"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselSection" role="button"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.auth-layout>
