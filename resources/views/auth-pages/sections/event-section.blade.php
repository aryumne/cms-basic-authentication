<x-layouts.auth-layout>
    <div class="row">
        <x-elements.nav-tab />
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>Event Section</h4>
                </div>
                <div class="card-body">
                    <x-elements.alert />
                    <form class="form" action="{{ route('event-section.update', $data->id) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <x-elements.input name-ID="event_title" label="Title" placeholder="section title" :value="$data->event_title" />
                        <div class="mb-3">
                            <label for="event_desc" class="form-label">Description</label>
                            <textarea id="event_desc" name="event_desc">
                                {{ $data->event_desc ?? '' }}
                            </textarea>
                        </div>
                        <x-elements.input name-ID="event_desc2" label="Desctiption 2" :value="$data->event_desc2" />
                        <x-elements.input type="file" name-ID="event_src_img" label="Upload Image" />
                        <x-elements.button type="submit" class="mt-3">Save changes</x-elements.button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>Preview</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 col-md-7 col-12">
                            <div id="preview-section-1">
                                <h5>{{ $data->event_title }}</h5>
                                {!! $data->event_desc ?? '' !!}
                                <p>{{ $data->event_desc2 }}</p>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5 col-12">
                            <x-elements.image src="{{ asset('storage/'.$data->event_src_img) }}"
                                alt="{{ $data->event_alt_img }}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            ClassicEditor
                .create(document.querySelector('#event_desc'), {
                    toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
                    heading: {
                        options: [{
                                model: 'paragraph',
                                title: 'Paragraph',
                                class: 'ck-heading_paragraph'
                            },
                            {
                                model: 'heading1',
                                view: 'h1',
                                title: 'Heading 1',
                                class: 'ck-heading_heading1'
                            },
                            {
                                model: 'heading2',
                                view: 'h2',
                                title: 'Heading 2',
                                class: 'ck-heading_heading2'
                            },
                            {
                                model: 'heading3',
                                view: 'h3',
                                title: 'Heading 3',
                                class: 'ck-heading_heading3'
                            },
                            {
                                model: 'heading4',
                                view: 'h4',
                                title: 'Heading 4',
                                class: 'ck-heading_heading4'
                            }
                        ]
                    }
                }).catch(error => {
                    console.error(error);
                });
        </script>
    @endpush
</x-layouts.auth-layout>
