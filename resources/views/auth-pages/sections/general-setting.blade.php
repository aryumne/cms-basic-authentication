<x-layouts.auth-layout>
    <div class="row">
        <x-elements.nav-tab />
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>General Setting</h4>
                </div>
                <div class="card-body">
                    <x-elements.alert />
                    <form class="form" action="{{ route('general-setting.update', $data->id) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="col-lg-6 col-12 px-4">
                                <div class="mb-3">
                                    <label for="address" class="form-label">Address</label>
                                    <textarea class="form-control" id="address" name="address" rows="3" required>{{ $data->address }}</textarea>
                                </div>
                                <x-elements.input name-ID="phone" label="Phone" placeholder="phone number"
                                    :value="$data->phone" required="true" />
                                <x-elements.input name-ID="email" label="Email" placeholder="email" :value="$data->email"
                                    required="true" />
                                <x-elements.input name-ID="footer_text" label="Footer Text" placeholder="footer text"
                                    :value="$data->footer_text" required="true" />
                                <x-elements.input name-ID="galery_title" label="Galery Section Title"
                                    placeholder="section title" :value="$data->galery_title" required="true" />
                                <div class="d-flex align-items-start align-items-sm-center gap-4 mb-3">
                                    <img src="{{ asset('storage/'.$data->brand_img )}}" alt="user-avatar"
                                        class="d-block rounded bg-primary" height="50" width="50"
                                        id="uploadedAvatar" />
                                    <div class="row" style="width: 100%">
                                        <x-elements.input type="file" name-ID="brand_img"
                                            label="Upload new brand logo" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-12 px-4">
                                <div class="d-flex align-items-start align-items-sm-center gap-4">
                                    <img src="{{ asset('storage/'.$data->instagram_media['icon']) }}" alt="user-avatar"
                                        class="d-block rounded" height="50" width="50" id="uploadedAvatar" />
                                    <div class="row" style="width: 100%">
                                        <x-elements.input type="file" name-ID="instagram_icon" label="Instagram media"
                                            style="margin-bottom: 0.5rem !important" />
                                        <div class="mb-3">
                                            <input type="text" class="form-control" id="instagram_url"
                                                name="instagram_url" value="{{ $data->instagram_media['url'] }}" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start align-items-sm-center gap-4">
                                    <img src="{{ asset('storage/'.$data->facebook_media['icon']) }}" alt="user-avatar"
                                        class="d-block rounded" height="50" width="50" id="uploadedAvatar" />
                                    <div class="row" style="width: 100%">
                                        <x-elements.input type="file" name-ID="facebook_icon" label="Facebook media"
                                            style="margin-bottom: 0.5rem !important" />
                                        <div class="mb-3">
                                            <input type="text" class="form-control" id="facebook_url"
                                                name="facebook_url" value="{{ $data->facebook_media['url'] }}" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start align-items-sm-center gap-4">
                                    <img src="{{ asset('storage/'.$data->twitter_media['icon']) }}" alt="user-avatar"
                                        class="d-block rounded" height="50" width="50" id="uploadedAvatar" />
                                    <div class="row" style="width: 100%">
                                        <x-elements.input type="file" name-ID="twitter_icon" label="Twitter media"
                                            style="margin-bottom: 0.5rem !important" />
                                        <div class="mb-3">
                                            <input type="text" class="form-control" id="twitter_url"
                                                name="twitter_url" value="{{ $data->twitter_media['url'] }}" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start align-items-sm-center gap-4">
                                    <img src="{{ asset('storage/'.$data->youtube_media['icon']) }}" alt="user-avatar"
                                        class="d-block rounded" height="50" width="50" id="uploadedAvatar" />
                                    <div class="row" style="width: 100%">
                                        <x-elements.input type="file" name-ID="youtube_icon" label="Youtube media"
                                            style="margin-bottom: 0.5rem !important" />
                                        <div class="mb-3">
                                            <input type="text" class="form-control" id="youtube_url"
                                                name="youtube_url" value="{{ $data->youtube_media['url'] }}" required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <x-elements.button type="submit" class="mt-3">Save changes</x-elements.button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-layouts.auth-layout>
