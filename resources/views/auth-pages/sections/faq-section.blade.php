<x-layouts.auth-layout>
    <div class="row">
        <x-elements.nav-tab />
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>FAQ Section</h4>
                </div>
                <div class="card-body">
                    <x-elements.alert />
                    <div class="row mb-3">
                        <form class="form" action="{{ route('faq-section.update', $data->id) }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <x-elements.input name-ID="faq_title" label="Title" placeholder="section title"
                                :value="$data->faq_title" required="true" />
                            <x-elements.button type="submit">Save changes</x-elements.button>
                        </form>
                    </div>
                    <div class="row text-end">
                        <div class="col">
                            <x-elements.button type="button" color="success" data-bs-toggle="modal"
                                data-bs-target="#addForm">New Faq</x-elements.button>
                            {{-- Modal --}}
                            <div class="modal fade" id="addForm" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel1">New FAQ 
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <form class="form" action="{{ route('faq-section.store') }}"
                                            method="POST">
                                            @csrf
                                            <div class="modal-body text-start">
                                                <div class="mb-3">
                                                    <label for="faq_question" class="form-label">Question</label>
                                                    <textarea class="form-control" id="faq_question" name="faq_question" rows="3" required>{{ old('faq_question') }}</textarea>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="faq_answer" class="form-label">Answer</label>
                                                    <textarea class="form-control" id="faq_answer" name="faq_answer" rows="3" required>{{ old('faq_answer') }}</textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <x-elements.button type="submit" color="primary" class="mb-2">
                                                    Save
                                                </x-elements.button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            {{-- End Modal --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive-lg">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Question</th>
                                        <th>Answer</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($faqs as $faq)
                                        @if ($loop->index > 0)
                                            <tr>
                                                <td>
                                                    {{ $faq->faq_question }}
                                                </td>
                                                <td class="text-wrap">
                                                    {{ $faq->faq_answer }}
                                                </td>
                                                <td style="width: 13rem;">
                                                    <x-elements.button type="button" color="warning"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#editform-{{ $loop->index }}">
                                                        Edit
                                                    </x-elements.button>
                                                    <form action="{{ route('faq-section.destroy', $faq->id) }}"
                                                        method="POST" id="delete-form-{{ $loop->index }}"
                                                        class="d-inline-block">
                                                        @csrf
                                                        @method('DELETE')
                                                        <x-elements.button type="submit" color="danger" class="mt-1">
                                                            Delete
                                                        </x-elements.button>
                                                    </form>
                                                </td>
                                            </tr>
                                            {{-- Modal --}}
                                            <div class="modal fade" id="editform-{{ $loop->index }}" tabindex="-1"
                                                aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel1">Update data
                                                            </h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <form class="form"
                                                            action="{{ route('faq-section.update', $faq->id) }}"
                                                            method="POST">
                                                            @csrf
                                                            @method('PATCH')
                                                            <div class="modal-body">
                                                                <div class="mb-3">
                                                                    <label for="faq_question"
                                                                        class="form-label">Question</label>
                                                                    <textarea class="form-control" id="faq_question" name="faq_question" rows="3" required>{{ $faq->faq_question }}</textarea>
                                                                </div>
                                                                <div class="mb-3">
                                                                    <label for="faq_answer"
                                                                        class="form-label">Answer</label>
                                                                    <textarea class="form-control" id="faq_answer" name="faq_answer" rows="3" required>{{ $faq->faq_answer }}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <x-elements.button type="submit" color="primary"
                                                                    class="mb-2">Save changes
                                                                </x-elements.button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- End Modal --}}
                                            @push('scripts')
                                                <script type="text/javascript">
                                                    document.addEventListener('DOMContentLoaded', function() {
                                                        let form = document.getElementById('delete-form-{{ $loop->index }}');

                                                        form.addEventListener('submit', function(event) {
                                                            event.preventDefault();
                                                            let itemId = form.getAttribute('data-id');

                                                            Swal.fire({
                                                                title: "Are you sure?",
                                                                text: "You won't be able to revert this!",
                                                                icon: "warning",
                                                                showCancelButton: !0,
                                                                confirmButtonText: "Yes, delete it!",
                                                                customClass: {
                                                                    confirmButton: "btn btn-primary me-3",
                                                                    cancelButton: "btn btn-label-secondary",
                                                                },
                                                                buttonsStyling: !1,
                                                            }).then(function(t) {
                                                                t.value && form.submit();
                                                            });

                                                        });
                                                    });
                                                </script>
                                            @endpush
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>Preview</h4>
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ $data->faq_title }}</h5>
                    <div class="row ">
                        @foreach ($faqs as $faq)
                            @if ($loop->index > 0)
                                <div class="col-12 mb-3">
                                    <div class="card">
                                        <div class="card-header bg-label-secondary py-2 px-0">
                                            <a class="d-flex justify-content-between align-items-center btn"
                                                data-bs-toggle="collapse" href="#collapse-{{ $loop->index }}"
                                                role="button" aria-expanded="{{ $loop->index == 1 ? true : false }}"
                                                aria-controls="collapse-{{ $loop->index }}">
                                                <div>
                                                    <p class="d-inline">
                                                        <strong>{{ $faq->faq_question }}</strong>
                                                    </p>
                                                </div>
                                                <div>
                                                    <x-elements.button type="button" color="transparent"
                                                        class="btn-icon d-inline">
                                                        <i class='bx bx-plus-medical'></i>
                                                    </x-elements.button>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="collapse {{ $loop->index == 1 ? 'show' : '' }}"
                                            id="collapse-{{ $loop->index }}">
                                            <div class="card-body py-2 pt-3">
                                                <p>{{ $faq->faq_answer }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.auth-layout>
