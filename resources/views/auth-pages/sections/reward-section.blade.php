<x-layouts.auth-layout>
    <div class="row">
        <x-elements.nav-tab />
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>Reward Section</h4>
                </div>
                <div class="card-body">
                   <x-elements.alert />
                     <form class="form" action="{{ route('reward-section.update', $data->id) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <x-elements.input name-ID="reward_title" label="Title" placeholder="section title" :value="$data->reward_title" required="true"/>
                        <x-elements.input type="file" name-ID="reward_card_img1" label="Upload Image Card 1" />
                        <x-elements.input name-ID="reward_card_desc1" label="Description Card 1" placeholder="section title" :value="$data->card_list[0]['reward_card_desc']" required="true" />
                        <x-elements.input type="file" name-ID="reward_card_img2" label="Upload Image Card 2" />
                        <x-elements.input name-ID="reward_card_desc2" label="Description Card 2" placeholder="section title" :value="$data->card_list[1]['reward_card_desc']" required="true" />
                        <x-elements.button type="submit" class="mt-3">Save changes</x-elements.button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>Preview</h4>
                </div>
                <div class="card-body">
                    <h5 class="card-title ps-4">{{ $data->reward_title }}</h5>
                    <div class="row row-cols-1 row-cols-md-3 g-4 justify-content-center">
                        @foreach ($data->card_list as $card)
                            <div class="col">
                                <div class="card">
                                    <div class="card-body text-center">
                                         <h5 class="card-title">Card {{ $loop->iteration }}</h5>
                                        <div style="overflow:hidden">
                                        <img src="{{ asset('storage/'.$card['reward_src_img']) }}" style="height: 300px; max-width: 100%;" alt="{{ $card['reward_alt_img'] }}">
                                        </div>
                                        <p class="card-text p-0">{{ $card['reward_card_desc'] }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.auth-layout>
