<x-layouts.auth-layout>
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>Galery Participant</h4>
                </div>
                <div class="card-body">
                    <div class="row mb-4">
                        <x-elements.alert />
                        <!-- Button trigger modal -->
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal">
                                    New Post
                                </button>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add new post</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                        </button>
                                    </div>
                                    <form class="form" method="POST" action="{{ route('galery.store') }}"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <x-elements.input type="file" name-ID="post_img" label="Post Image" />
                                            <x-elements.input name-ID="username" label="Username"
                                                placeholder="@username" required="true" />
                                            <x-elements.input type="number" name-ID="likes" label="Likes"
                                                placeholder="123" required="true" />
                                            <div class="mb-3">
                                                <label for="desc" class="form-label">Caption</label>
                                                <textarea class="form-control" id="desc" name="desc" rows="2" required></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="exampleFormControlSelect1" class="form-label">Participant
                                                    Type</label>
                                                <select class="form-select" id="exampleFormControlSelect1"
                                                    aria-label="Default select example">
                                                    <option value="mekanik">Mekanik</option>
                                                    <option value="konsumen">Konsumen</option>
                                                </select>
                                            </div>
                                            <div class="form-check mb-3">
                                                <input class="form-check-input" type="checkbox" id="is_winner"
                                                    name="is_winner" />
                                                <label class="form-check-label" for="is_winner">
                                                    Winner
                                                </label>
                                            </div>
                                            <div id="togglewinner">
                                                <x-elements.input type="number" name-ID="ranking" label="Winner"
                                                    placeholder="123" />
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row row-cols-1 row-cols-md-3 g-4 mt-0">
                            @foreach ($galeries as $galery)
                                <div class="col">
                                    <div class="card h-100">
                                        <img class="card-img-top" src="{{ asset('storage/' . $galery->post_img) }}"
                                            alt="Card image cap" />
                                        <div class="card-body">
                                            <div class="d-flex justify-content-between">
                                                <h5 class="card-title">{{ $galery->username }}</h5>
                                                <h5 class="card-title">{{ $galery->likes }} likes</h5>
                                            </div>
                                            <p class="card-text">{{ $galery->desc }}</p>
                                            <a href="#" class="card-link text-warning">Edit</a>
                                            <a href="#" class="card-link text-danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            var checkbox = document.getElementById('is_winner');
            var elementToHide = document.getElementById('togglewinner');
            elementToHide.style.display = 'none';

            checkbox.addEventListener('change', function() {
                if (checkbox.checked) {
                    elementToHide.style.display = 'block';
                } else {
                    elementToHide.style.display = 'none';
                }
            });
        </script>
    @endpush
</x-layouts.auth-layout>
